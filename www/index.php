<?php
	require_once('../helpers/engine.php');
	$engine = new Engine();
	$images = $engine->getImages();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="style.css">
		<title>Čekuj mámu!</title>
	</head>

	<body>
	<h1>Čekuj mámu!</h1>
	<h2>Která je lepší? Klikej!</h2>

	<center>
		<table>
			<tr>
				<td valign="top" class="image">
					<form action="/" method="post">
						<input type="hidden" name="w" value="<?= $images[0]->id?>">
						<input type="hidden" name="l" value="<?= $images[1]->id?>">
						<input type="image" src="images/<?= $images[0]->filename?>" />
					</form>
				</td>

				<td valign="top" class="image">
					<form action="/" method="post">
						<input type="hidden" name="w" value="<?= $images[1]->id?>">
						<input type="hidden" name="l" value="<?= $images[0]->id?>">
						<input type="image" src="images/<?= $images[1]->filename?>" />
					</form>
				</td>
			</tr>
			<tr>
				<td>Vyhrála: <strong><?= $images[0]->wins?>x</strong>, Prohrála: <strong><?= $images[0]->losses?>x</strong></td>
				<td>Vyhrála: <strong><?= $images[1]->wins?>x</strong>, Prohrála: <strong><?= $images[1]->losses?>x</strong></td>
			</tr>
			<tr>
				<td>Skóre: <strong><?= $images[0]->score?></strong></td>
				<td>Skóre: <strong><?= $images[1]->score?></strong></td>
			</tr>
		</table>
	</center>

	</body>
</html>
