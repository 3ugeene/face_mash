<?php

require_once('db.php');

if ($handler = opendir('../www/images')) {
	while (($file = readdir($handler)) !== FALSE) {
		if(substr($file, 0, 1) != '.')
			$images[]['filename'] = $file;
	}
	closedir($handler);
}


foreach ($images as $image) {
	if (!dibi::query("INSERT INTO [images]", $image))
		print "err inserting image {$image['filename']}\n";
	else
		print "finished installing image {$image['filename']}\n";
}
