<?php
require_once('db.php');
require_once('elo_functions.php');

class Engine 
{
	private $images = NULL;

	public function __construct()
	{
		$this->images = $this->getImagesFromDb();
		$this->checkImages($this->images);
		$this->battle();
	}

	public function getImages()
	{
		return $this->images;
	}

	private function getImagesFromDb()
	{
		$images = NULL;

		$random_images = dibi::query("SELECT * FROM [images] ORDER BY RAND() LIMIT 0, 2")->fetchAll();
		foreach ($random_images as $id => $image)
			$images[] = $image;

		return $images;
	}

	private function checkImages($images)
	{
		if($images === NULL || sizeof($images) < 2)
			die('No images installed. Sorry :(');
	}

	private function battle()
	{
		if ( isset($_POST['w'], $_POST['l']) )
		{
			$w = (int)$_POST['w'];
			$l = (int)$_POST['l'];

			$duel = $this->getDuel($w, $l);
			$elo = new \Elo($duel);

			$this->updateStatuses($duel, $elo);
		}
	}

	private function getDuel($w_id, $l_id)
	{
		return 
		[
			'winner' => dibi::query('SELECT * FROM [images] WHERE `id` = %i', $w_id)->fetch(),
			'loser' => dibi::query('SELECT * FROM [images] WHERE `id` = %i', $l_id)->fetch()
		];
	}

	private function updateStatuses($duel, $elo)
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		$winner_id = $duel['winner']->id;
		$loser_id = $duel['loser']->id;

		dibi::query('UPDATE [images] SET `score` = %i', $elo->getWinnerNewScore(), ', `wins` = `wins` + 1 WHERE `id` = %i', $winner_id);
		dibi::query('UPDATE [images] SET `score` = %i', $elo->getLoserNewScore(), ', `losses` = `losses` + 1 WHERE `id` = %i', $loser_id);
		dibi::query("INSERT INTO [battles] SET `winner` = %i", $winner_id, ", `loser` = %i", $loser_id, ", `ip` = %s", $ip);
	}
}