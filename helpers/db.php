<?php
date_default_timezone_set("Europe/Prague");

require_once('dibi/dibi.min.php');

try {
	dibi::connect(array(
	    'driver'   => 'mysql',
	    'host'     => 'localhost',
	    'username' => 'root',
	    'password' => 'root',
	    'database' => 'mash',
	));
} catch (DibiException $e) {
	echo "Err connecting to database.\n";
	die;
}