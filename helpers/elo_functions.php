<?php
//based on https://www.youtube.com/watch?v=1_xLjI6zN3I&spfreload=10

class Elo 
{
	private $k;
	private $duel;

	public function __construct($duel, $k = 25)
	{
		$this->k = $k;
		$this->duel = $duel;
	}

	public function getWinnerNewScore()
	{
		$winner_expected = $this->expected($this->duel['loser']->score, $this->duel['winner']->score);
		return $this->win($this->duel['winner']->score, $winner_expected);
	}

	public function getLoserNewScore()
	{
		$loser_expected = $this->expected($this->duel['winner']->score, $this->duel['loser']->score);
		return $this->loss($this->duel['loser']->score, $loser_expected);
	}

	private function expected($Rb, $Ra)
	{
		return 1 / (1 + pow(10, ($Rb - $Ra) / 400));
	}

	private function win($score, $expected)
	{
		return $score + ($this->k * (2 - $expected));
	}

	private function loss($score, $expected)
	{
		return $score - ($this->k * (2 - $expected));
	}
}

