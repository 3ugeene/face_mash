CREATE TABLE `battles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `winner` bigint(20) unsigned NOT NULL,
  `loser` bigint(20) unsigned NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `winner` (`winner`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;


CREATE TABLE `images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `score` int(10) unsigned NOT NULL DEFAULT '1500',
  `wins` int(10) unsigned NOT NULL DEFAULT '0',
  `losses` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;


images -> www/images folder
run install_images.php